core = 7.x
api = 2

; uw_water_pub
projects[uw_water_pub][type] = "module"
projects[uw_water_pub][download][type] = "git"
projects[uw_water_pub][download][url] = "https://git.uwaterloo.ca/j4truong/uw_water_pub.git"
projects[uw_water_pub][download][tag] = "7.x-1.0"
projects[uw_water_pub][subdir] = ""
